// vite.config.ts
import { fileURLToPath, URL } from "node:url";
import vue from "file:///C:/Users/VALERY/detect_photo_vue/node_modules/@vitejs/plugin-vue/dist/index.mjs";
import AutoImport from "file:///C:/Users/VALERY/detect_photo_vue/node_modules/unplugin-auto-import/dist/vite.js";
import { NaiveUiResolver } from "file:///C:/Users/VALERY/detect_photo_vue/node_modules/unplugin-vue-components/dist/resolvers.js";
import Components from "file:///C:/Users/VALERY/detect_photo_vue/node_modules/unplugin-vue-components/dist/vite.js";
import { defineConfig, loadEnv } from "file:///C:/Users/VALERY/detect_photo_vue/node_modules/vite/dist/node/index.js";
import pluginSvgVue from "file:///C:/Users/VALERY/detect_photo_vue/node_modules/@vuetter/vite-plugin-vue-svg/dist/index.js";
var __vite_injected_original_import_meta_url = "file:///C:/Users/VALERY/detect_photo_vue/vite.config.ts";
var vite_config_default = defineConfig(({ mode }) => {
  process.env = { ...process.env, ...loadEnv(mode, process.cwd()) };
  let plugins = [
    vue({ script: { defineModel: true, propsDestructure: true } }),
    pluginSvgVue(),
    AutoImport({
      dts: "src/shared/types/auto-imports.d.ts",
      imports: [
        "vue",
        "pinia",
        "vitest",
        "vue-router",
        {
          "naive-ui": [
            "useDialog",
            "useMessage",
            "useNotification",
            "useLoadingBar"
          ],
          "@tanstack/vue-query": [
            "useQuery",
            "useQueries",
            "useMutation",
            "useIsFetching",
            "useQueryClient",
            "VueQueryPlugin"
          ]
        }
      ],
      eslintrc: {
        enabled: true
      }
    }),
    Components({
      dirs: [],
      dts: "src/shared/types/components.d.ts",
      resolvers: [NaiveUiResolver()]
    })
  ];
  return {
    plugins,
    assetsInclude: ["**/*.mp4"],
    server: {
      port: 5173
    },
    resolve: {
      alias: {
        "@": fileURLToPath(new URL("./src", __vite_injected_original_import_meta_url)),
        "helpers": fileURLToPath(new URL("./src/helpers", __vite_injected_original_import_meta_url))
      }
    }
  };
});
export {
  vite_config_default as default
};
//# sourceMappingURL=data:application/json;base64,ewogICJ2ZXJzaW9uIjogMywKICAic291cmNlcyI6IFsidml0ZS5jb25maWcudHMiXSwKICAic291cmNlc0NvbnRlbnQiOiBbImNvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9kaXJuYW1lID0gXCJDOlxcXFxVc2Vyc1xcXFxWQUxFUllcXFxcZGV0ZWN0X3Bob3RvX3Z1ZVwiO2NvbnN0IF9fdml0ZV9pbmplY3RlZF9vcmlnaW5hbF9maWxlbmFtZSA9IFwiQzpcXFxcVXNlcnNcXFxcVkFMRVJZXFxcXGRldGVjdF9waG90b192dWVcXFxcdml0ZS5jb25maWcudHNcIjtjb25zdCBfX3ZpdGVfaW5qZWN0ZWRfb3JpZ2luYWxfaW1wb3J0X21ldGFfdXJsID0gXCJmaWxlOi8vL0M6L1VzZXJzL1ZBTEVSWS9kZXRlY3RfcGhvdG9fdnVlL3ZpdGUuY29uZmlnLnRzXCI7aW1wb3J0IHsgZmlsZVVSTFRvUGF0aCwgVVJMIH0gZnJvbSAnbm9kZTp1cmwnO1xuXG5pbXBvcnQgdnVlIGZyb20gJ0B2aXRlanMvcGx1Z2luLXZ1ZSc7XG5pbXBvcnQgQXV0b0ltcG9ydCBmcm9tICd1bnBsdWdpbi1hdXRvLWltcG9ydC92aXRlJztcbmltcG9ydCB7IE5haXZlVWlSZXNvbHZlciB9IGZyb20gJ3VucGx1Z2luLXZ1ZS1jb21wb25lbnRzL3Jlc29sdmVycyc7XG5pbXBvcnQgQ29tcG9uZW50cyBmcm9tICd1bnBsdWdpbi12dWUtY29tcG9uZW50cy92aXRlJztcbmltcG9ydCB7IGRlZmluZUNvbmZpZywgbG9hZEVudiB9IGZyb20gJ3ZpdGUnO1xuXG5pbXBvcnQgcGx1Z2luU3ZnVnVlIGZyb20gJ0B2dWV0dGVyL3ZpdGUtcGx1Z2luLXZ1ZS1zdmcnO1xuXG5leHBvcnQgZGVmYXVsdCBkZWZpbmVDb25maWcoKHsgbW9kZSB9KSA9PiB7XG4gIHByb2Nlc3MuZW52ID0geyAuLi5wcm9jZXNzLmVudiwgLi4ubG9hZEVudihtb2RlLCBwcm9jZXNzLmN3ZCgpKSB9O1xuXG4gIGxldCBwbHVnaW5zID0gW1xuICAgIHZ1ZSh7IHNjcmlwdDogeyBkZWZpbmVNb2RlbDogdHJ1ZSwgcHJvcHNEZXN0cnVjdHVyZTogdHJ1ZSB9IH0pLFxuICAgIHBsdWdpblN2Z1Z1ZSgpLFxuICAgIEF1dG9JbXBvcnQoe1xuICAgICAgZHRzOiAnc3JjL3NoYXJlZC90eXBlcy9hdXRvLWltcG9ydHMuZC50cycsXG4gICAgICBpbXBvcnRzOiBbXG4gICAgICAgICd2dWUnLFxuICAgICAgICAncGluaWEnLFxuICAgICAgICAndml0ZXN0JyxcbiAgICAgICAgJ3Z1ZS1yb3V0ZXInLFxuICAgICAgICB7XG4gICAgICAgICAgJ25haXZlLXVpJzogW1xuICAgICAgICAgICAgJ3VzZURpYWxvZycsXG4gICAgICAgICAgICAndXNlTWVzc2FnZScsXG4gICAgICAgICAgICAndXNlTm90aWZpY2F0aW9uJyxcbiAgICAgICAgICAgICd1c2VMb2FkaW5nQmFyJyxcbiAgICAgICAgICBdLFxuICAgICAgICAgICdAdGFuc3RhY2svdnVlLXF1ZXJ5JzogW1xuICAgICAgICAgICAgJ3VzZVF1ZXJ5JyxcbiAgICAgICAgICAgICd1c2VRdWVyaWVzJyxcbiAgICAgICAgICAgICd1c2VNdXRhdGlvbicsXG4gICAgICAgICAgICAndXNlSXNGZXRjaGluZycsXG4gICAgICAgICAgICAndXNlUXVlcnlDbGllbnQnLFxuICAgICAgICAgICAgJ1Z1ZVF1ZXJ5UGx1Z2luJyxcbiAgICAgICAgICBdLFxuICAgICAgICB9LFxuICAgICAgXSxcbiAgICAgIGVzbGludHJjOiB7XG4gICAgICAgIGVuYWJsZWQ6IHRydWUsXG4gICAgICB9LFxuICAgIH0pLFxuICAgIENvbXBvbmVudHMoe1xuICAgICAgZGlyczogW10sXG4gICAgICBkdHM6ICdzcmMvc2hhcmVkL3R5cGVzL2NvbXBvbmVudHMuZC50cycsXG4gICAgICByZXNvbHZlcnM6IFtOYWl2ZVVpUmVzb2x2ZXIoKV0sXG4gICAgfSksXG4gIF07XG5cbiAgcmV0dXJuIHtcbiAgICBwbHVnaW5zLFxuICAgIGFzc2V0c0luY2x1ZGU6IFsnKiovKi5tcDQnXSxcbiAgICBzZXJ2ZXI6IHtcbiAgICAgIHBvcnQ6IDUxNzMsXG4gICAgfSxcbiAgICByZXNvbHZlOiB7XG4gICAgICBhbGlhczoge1xuICAgICAgICAnQCc6IGZpbGVVUkxUb1BhdGgobmV3IFVSTCgnLi9zcmMnLCBpbXBvcnQubWV0YS51cmwpKSxcbiAgICAgICAgJ2hlbHBlcnMnOiBmaWxlVVJMVG9QYXRoKG5ldyBVUkwoJy4vc3JjL2hlbHBlcnMnLCBpbXBvcnQubWV0YS51cmwpKSxcbiAgICAgIH0sXG4gICAgfSxcbiAgfTtcbn0pO1xuIl0sCiAgIm1hcHBpbmdzIjogIjtBQUEwUixTQUFTLGVBQWUsV0FBVztBQUU3VCxPQUFPLFNBQVM7QUFDaEIsT0FBTyxnQkFBZ0I7QUFDdkIsU0FBUyx1QkFBdUI7QUFDaEMsT0FBTyxnQkFBZ0I7QUFDdkIsU0FBUyxjQUFjLGVBQWU7QUFFdEMsT0FBTyxrQkFBa0I7QUFSc0osSUFBTSwyQ0FBMkM7QUFVaE8sSUFBTyxzQkFBUSxhQUFhLENBQUMsRUFBRSxLQUFLLE1BQU07QUFDeEMsVUFBUSxNQUFNLEVBQUUsR0FBRyxRQUFRLEtBQUssR0FBRyxRQUFRLE1BQU0sUUFBUSxJQUFJLENBQUMsRUFBRTtBQUVoRSxNQUFJLFVBQVU7QUFBQSxJQUNaLElBQUksRUFBRSxRQUFRLEVBQUUsYUFBYSxNQUFNLGtCQUFrQixLQUFLLEVBQUUsQ0FBQztBQUFBLElBQzdELGFBQWE7QUFBQSxJQUNiLFdBQVc7QUFBQSxNQUNULEtBQUs7QUFBQSxNQUNMLFNBQVM7QUFBQSxRQUNQO0FBQUEsUUFDQTtBQUFBLFFBQ0E7QUFBQSxRQUNBO0FBQUEsUUFDQTtBQUFBLFVBQ0UsWUFBWTtBQUFBLFlBQ1Y7QUFBQSxZQUNBO0FBQUEsWUFDQTtBQUFBLFlBQ0E7QUFBQSxVQUNGO0FBQUEsVUFDQSx1QkFBdUI7QUFBQSxZQUNyQjtBQUFBLFlBQ0E7QUFBQSxZQUNBO0FBQUEsWUFDQTtBQUFBLFlBQ0E7QUFBQSxZQUNBO0FBQUEsVUFDRjtBQUFBLFFBQ0Y7QUFBQSxNQUNGO0FBQUEsTUFDQSxVQUFVO0FBQUEsUUFDUixTQUFTO0FBQUEsTUFDWDtBQUFBLElBQ0YsQ0FBQztBQUFBLElBQ0QsV0FBVztBQUFBLE1BQ1QsTUFBTSxDQUFDO0FBQUEsTUFDUCxLQUFLO0FBQUEsTUFDTCxXQUFXLENBQUMsZ0JBQWdCLENBQUM7QUFBQSxJQUMvQixDQUFDO0FBQUEsRUFDSDtBQUVBLFNBQU87QUFBQSxJQUNMO0FBQUEsSUFDQSxlQUFlLENBQUMsVUFBVTtBQUFBLElBQzFCLFFBQVE7QUFBQSxNQUNOLE1BQU07QUFBQSxJQUNSO0FBQUEsSUFDQSxTQUFTO0FBQUEsTUFDUCxPQUFPO0FBQUEsUUFDTCxLQUFLLGNBQWMsSUFBSSxJQUFJLFNBQVMsd0NBQWUsQ0FBQztBQUFBLFFBQ3BELFdBQVcsY0FBYyxJQUFJLElBQUksaUJBQWlCLHdDQUFlLENBQUM7QUFBQSxNQUNwRTtBQUFBLElBQ0Y7QUFBQSxFQUNGO0FBQ0YsQ0FBQzsiLAogICJuYW1lcyI6IFtdCn0K
