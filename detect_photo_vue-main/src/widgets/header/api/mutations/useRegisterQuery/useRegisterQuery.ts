import { createUserWithEmailAndPassword } from 'firebase/auth';

import type { RegisterMutate } from '../../../types';

const useRegisterQuery = (options = {}) =>
  useMutation({
    mutationFn: (payload: RegisterMutate) =>
      createUserWithEmailAndPassword(
        payload.auth,
        payload.email,
        payload.password,
      ),
    ...options,
  });

export default useRegisterQuery;
