import type { RouteRecordRaw } from 'vue-router';

import { getCurrentUser } from 'vuefire';

export const trainPage = {
  path: 'train',
  name: 'train',
  component: () => import('./TrainPage.vue'),
} as RouteRecordRaw;
