import type { RouteRecordRaw } from 'vue-router';

export const detectVideoPage = {
  path: '',
  name: 'video',
  component: () => import('./DetectVideoPage.vue'),
} as RouteRecordRaw;
