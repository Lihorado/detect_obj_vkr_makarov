import type { RouteRecordRaw } from 'vue-router';

export const datasetPage: RouteRecordRaw = {
  path: '/dataset/:id',
  name: 'dataset',
  component: () => import('./DatasetPage.vue'),
};
