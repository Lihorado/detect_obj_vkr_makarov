import type { RouteRecordRaw } from 'vue-router';
import { getCurrentUser } from 'vuefire';

export const homePage = {
  path: 'photo',
  name: 'photo',
  component: () => import('./HomePage.vue'),
} as RouteRecordRaw;
