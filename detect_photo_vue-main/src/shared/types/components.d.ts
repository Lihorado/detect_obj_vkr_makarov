/* eslint-disable */
/* prettier-ignore */
// @ts-nocheck
// Generated by unplugin-vue-components
// Read more: https://github.com/vuejs/core/pull/3399
export {}

declare module 'vue' {
  export interface GlobalComponents {
    NButton: typeof import('naive-ui')['NButton']
    NCard: typeof import('naive-ui')['NCard']
    NConfigProvider: typeof import('naive-ui')['NConfigProvider']
    NDialogProvider: typeof import('naive-ui')['NDialogProvider']
    NDivider: typeof import('naive-ui')['NDivider']
    NFlex: typeof import('naive-ui')['NFlex']
    NForm: typeof import('naive-ui')['NForm']
    NFormItem: typeof import('naive-ui')['NFormItem']
    NGradientText: typeof import('naive-ui')['NGradientText']
    NIcon: typeof import('naive-ui')['NIcon']
    NImage: typeof import('naive-ui')['NImage']
    NImageGroup: typeof import('naive-ui')['NImageGroup']
    NImg: typeof import('naive-ui')['NImg']
    NInput: typeof import('naive-ui')['NInput']
    NLayout: typeof import('naive-ui')['NLayout']
    NLayoutContent: typeof import('naive-ui')['NLayoutContent']
    NLayoutFooter: typeof import('naive-ui')['NLayoutFooter']
    NLayoutHeader: typeof import('naive-ui')['NLayoutHeader']
    NLayoutSider: typeof import('naive-ui')['NLayoutSider']
    NList: typeof import('naive-ui')['NList']
    NListItem: typeof import('naive-ui')['NListItem']
    NLoadingBarProvider: typeof import('naive-ui')['NLoadingBarProvider']
    NMenu: typeof import('naive-ui')['NMenu']
    NMessageProvider: typeof import('naive-ui')['NMessageProvider']
    NModal: typeof import('naive-ui')['NModal']
    NNotificationProvider: typeof import('naive-ui')['NNotificationProvider']
    NSpace: typeof import('naive-ui')['NSpace']
    NSpin: typeof import('naive-ui')['NSpin']
    NSwitch: typeof import('naive-ui')['NSwitch']
    NText: typeof import('naive-ui')['NText']
    NThing: typeof import('naive-ui')['NThing']
    NUpload: typeof import('naive-ui')['NUpload']
    RouterLink: typeof import('vue-router')['RouterLink']
    RouterView: typeof import('vue-router')['RouterView']
  }
}
