const useTheme = defineStore('theme', () => {
  const themeDark = ref(false);

  return { themeDark };
});

export default useTheme;
