const useLogin = defineStore('login', () => {
  const showModal = ref(false);

  return {
    showModal,
  };
});

export default useLogin;
