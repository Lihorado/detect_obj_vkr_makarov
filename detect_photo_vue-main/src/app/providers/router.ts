import { createRouter, createWebHistory } from 'vue-router';

import { routes } from '@/pages';

import { isAccess } from '../middleware';

export const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
});

router.beforeEach(isAccess);
