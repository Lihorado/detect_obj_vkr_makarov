import type { RouteLocationNormalized, NavigationGuardNext } from 'vue-router';
import { useCurrentUser } from 'vuefire';

const isAccess = (
  to: RouteLocationNormalized,
  from: RouteLocationNormalized,
  next: NavigationGuardNext,
) => {
  // const user = useCurrentUser();

  // if (user && to.name === 'login') {
  //   next({ name: 'home' });
  // }

  next();
};

export default isAccess;
